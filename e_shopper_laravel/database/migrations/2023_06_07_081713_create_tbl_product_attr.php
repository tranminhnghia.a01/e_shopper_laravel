<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product_attr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id');
            $table->string('product_attr_name');
            $table->string('product_attr_qty');
            $table->string('product_attr_price');
            $table->string('product_attr_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product_attr');
    }
};
