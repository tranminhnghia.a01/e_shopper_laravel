@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
            <div class="alert alert-{{session('style')}}">
                {{ session('msg') }}
            </div>
      @endif
		<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Create Blog
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal form-material" id="signupForm" enctype="multipart/form-data" novalidate="novalidate" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Blog Title <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="blog_title" value="">
                                    @error('blog_title')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control form-control-line" name="blog_image">
                                    @error('blog_image')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Descriptions</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"   name="blog_des"></textarea>
                                    @error('blog_des')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Content</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line content_blog" id="content_blog" name="blog_content"></textarea>
                                    @error('blog_content')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success">Add blog</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    <div class="clearfix"> </div>

    </section>
@endsection