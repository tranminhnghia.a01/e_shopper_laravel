@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
		<div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create Blog
                </div>
                <div class="row w3-res-tb">
                    <div class="col-sm-5 m-b-xs">
                        <select class="input-sm form-control w-sm inline v-middle">
                            <option value="0">Bulk action</option>
                            <option value="1">Delete selected</option>
                            <option value="2">Bulk edit</option>
                            <option value="3">Export</option>
                        </select>
                        <button class="btn btn-sm btn-default">Apply</button>                
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;">
                                <label class="i-checks m-b-none">
                                    <input type="checkbox"><i></i>
                                </label>
                                </th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Descriptins</th>
                                <th style="width:30px;"></th>
                                <th style="width:30px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($blog_list as $value )
                            <tr>
                                <th scope="row">{{ $value->id }}</th>
                                <td>{{ $value->blog_title }}</td>
                                <td><img src="{{ asset('backend/upload/blogs/'.$value->blog_image) }}" alt="" style="width: 50px;"></td>
                                <td>{{ $value->blog_des }}</td>
                                <td>
                                    @if ($value->blog_status == 1)
                                        <a href="" class="alert alert-success"> Show</a>
                                    @else
                                        <a href="" class="alert alert-danger"> Hide</a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin.blog-edit',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i></a>
                                    <a href="{{ route('admin.blog-destroy',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-times text-danger text"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <a href="{{ route('admin.blog-create') }}" class="btn btn-success">Create Blog</a>
                    </div>
            </div>
        </div>
    </section>
   
@endsection