@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
					<div class="alert alert-{{session('style')}}">
						{{ session('msg') }}
					</div>
				@endif
		<div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Product infomation
                </div>
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;">
                                </th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Descriptins</th>
                                <th>Content</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                    $product_image = json_decode($product->product_image);
                                    $url_product_image = 'backend/upload/products/'.$product_image[0];
                                @endphp
                                <tr>
                                    <td class="id-product" scope="row">{{ $product->id }}</td>
                                    <td scope="row">{{ $product->product_name }}</td>
                                    <td><img src="{{ asset($url_product_image) }}" alt="" style="width: 100px;"></td>
                                    <td style="">{{ $product->product_des }}</td>
                                    <td style="">{{ $product->product_content }}</td>
                                    <td>
                                        @if ($product->product_status == 1)
                                            <a href="" class="alert-success"> Show</a>
                                        @else
                                            <a href="" class="alert-danger"> Hide</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('product.edit',$product->id) }}" class="active" ui-toggle-class=""><i class="fa fa-edit text-success text-active"></i></a>
                                    </td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row w3-res-tb">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <div class="panel-heading">
                            Product attributes
                        </div>
                    </div>
                    <div class="col-sm-4">
                       
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Attrbutes Name</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Price</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="product-attr">
                            @foreach ($product_info as $value )
                                {{-- @php
                                    $product_image = json_decode($value->product_image);
                                    $url_product_image = 'backend/upload/products/'.$product_image[0];
                                @endphp --}}
                                <tr>
                                    <th scope="row">{{ $value->product_id }}</th>
                                    <th scope="row">{{ $value->product_attr_name }}</th>
                                    {{-- <td><img src="{{ asset($url_product_image) }}" alt="" style="width: 100px;"></td> --}}
                                    <td style="">{{ $value->product_attr_qty }}</td>
                                    <td style="">{{ number_format($value->product_attr_price) }} vnđ</td>
                                    <td>
                                        @if ($value->product_attr_status == 1)
                                            <a href="" class="alert alert-success"> Show</a>
                                        @else
                                            <a href="" class="alert alert-danger"> Hide</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('product-attr.edit',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-edit text-success text-active"></i></a>
                                        <a href="{{ route('product-attr.destroy',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-times text-danger text"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="form-group">
                    <div class="col-sm-12">
                        <a href="{{ route('product-attr.create',$product->id) }}" class="btn btn-success">Add product attributes</a>
                        <a class="btn btn-success add-attrs">Add product attributes</a>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
@endsection