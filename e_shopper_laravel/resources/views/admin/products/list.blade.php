@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
					<div class="alert alert-{{session('style')}}">
						{{ session('msg') }}
					</div>
				@endif
		<div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create Blog
                </div>
                <div class="row w3-res-tb">
                    <div class="col-sm-5 m-b-xs">
                        <select class="input-sm form-control w-sm inline v-middle">
                            <option value="0">Bulk action</option>
                            <option value="1">Delete selected</option>
                            <option value="2">Bulk edit</option>
                            <option value="3">Export</option>
                        </select>
                        <button class="btn btn-sm btn-default">Apply</button>                
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Image</th>
                                <th scope="col">Category</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product as $value )
                                @php
                                    $product_image = json_decode($value->product_image)
                                    $url_product_image = 'backend/upload/products/'.$product_image[0]
                                    
                                @endphp
                                <tr>
                                    <th scope="row">{{ $value->id_product }}</th>
                                    <th scope="row">{{ $value->product_name }}</th>
                                    <td><img src="{{ asset($url_product_image) }}" alt="" style="width: 100px;"></td>
                                    <td style="">
                                        @foreach ($category as $item_cate) 
                                            @if ($item_cate->id == $value->id_category) 
                                                {{ $item_cate->category_name }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @if ($value->product_status == 1)
                                            <a href="" class="alert-success"> Show</a>
                                        @else
                                            <a href="" class="alert-danger"> Hide</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('product.show',$value->id) }}">Details</a> |
                                        <a href="{{ route('product.destroy',$value->id) }}" id="delete-handel" class="active" ui-toggle-class=""><i class="fa fa-times text-danger text"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <a href="{{ route('product.create') }}" class="btn btn-success">Add product</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
@endsection