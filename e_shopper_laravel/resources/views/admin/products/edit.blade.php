@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
            <div class="alert alert-{{session('style')}}">
                {{ session('msg') }}
            </div>
      @endif
		<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Update Product
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal form-material" enctype="multipart/form-data" method="POST" action="{{ route('product.update',$product->id) }}">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">product Id <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="product_id" name="id_product" value="{{ $product->id_product }}">
                                    @error('product_id')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">product Name <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="product_name" value="{{ $product->product_name }}">
                                    @error('product_name')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Product Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control form-control-line" name="product_image[]" multiple="multiple">
                                    @error('product_image')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" >
                                @php
                                    $image_product = json_decode($product->product_image);
                                @endphp 
                                    
                                <div class="col">
                                    @foreach ($image_product as $val)
                                        <img src="{{ asset('backend/upload/products/'.$val ) }}" alt="" style="width: 80px">
                                        <input type="checkbox" style="height: 20px;" value="{{$val}}" name="image_delete[]">
                                @endforeach

                                </div>
        
                        </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Descriptions</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"  name="product_des">{{ $product->product_des }}</textarea>
                                    @error('product_content')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Content</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"  name="product_content">{{ $product->product_content }}</textarea>
                                    @error('product_des')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status</label>
                                <div class="col-sm-6">
                                    @if ($product->product_status == 0)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="product_status" class="custom-control-input" checked value="0" >
                                        <label class="custom-control-label" for="customRadio1" >Hide</label>

                                        <input type="radio" id="customRadio2" name="product_status" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="customRadio2">Show</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="product_status" class="custom-control-input" value="0" >
                                        <label class="custom-control-label" for="customRadio1" >Hide</label>

                                        <input type="radio" id="customRadio2" name="product_status" class="custom-control-input" checked value="1">
                                        <label class="custom-control-label" for="customRadio2">Show</label>
                                    </div>
                                    @endif
                                    
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Category</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" name="id_category">
                                        <option  value="{{ $product->id_category }}" selected>{{ $get_name_category->category_name }}</option>
                                        @foreach ($parent_category as $parent_value )
                                            <option  value="{{ $parent_value->id  }}">--{{$parent_value->category_name}}--</option>
                                            @foreach ($list as $value_item)
                                                @if ($value_item->category_level == $parent_value->id)
                                                    <option  value="{{ $value_item->id  }}">----{{$value_item->category_name}}----</option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success">Save Product</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
        
    <div class="clearfix"> </div>
   
    </section>
@endsection


