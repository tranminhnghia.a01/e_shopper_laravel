@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
            <div class="alert alert-{{session('style')}}">
                {{ session('msg') }}
            </div>
      @endif
		<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Create Blog
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal form-material" enctype="multipart/form-data" method="POST" action="{{ route('product.store') }}">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">product Id <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="product_id" name="id_product" value="">
                                    @error('product_id')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">product Name <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="product_name" value="">
                                    @error('product_name')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Product Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control form-control-line" name="product_image[]" multiple="multiple">
                                    @error('product_image')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">product Price <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="product_price" value="">
                                    @error('product_price')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Descriptions</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"  name="product_des"></textarea>
                                    @error('product_content')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Content</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"  name="product_content"></textarea>
                                    @error('product_des')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status</label>
                                <div class="col-sm-6">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="product_status" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="customRadio1">Hide</label>

                                        <input type="radio" id="customRadio2" name="product_status" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="customRadio2">Show</label>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Size</label>
                                    @foreach ($size_list as $value)
                                    <div class="col-sm-1">
                                        <div class="custom-control custom-checkbox checkbox_size">
                                            <input type="checkbox" class="custom-control-input product_size" name="product_size[]" id="customCheck{{ $value->id }}" value="{{ $value->attr_value }}">
                                            <label class="custom-control-label" for="customCheck{{ $value->id }}">{{ $value->attr_value }}</label>
                                        </div>
                                    </div>
                                    @endforeach
                                    @error('product_size')
                                        <span style="color: red">{{ $message }}</span>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Color</label>
                                    @foreach ($color_list as $value)
                                    <div class="col-sm-1">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="checkbox" class="custom-control-input product_color" name="product_color[]" id="colorcustomCheck{{ $value->id }}" value="{{ $value->attr_value }}">
                                            <label class="custom-control-label" for="colorcustomCheck{{ $value->id }}">{{ $value->attr_value }}</label>
                                        </div>
                                    </div>
                                    @endforeach
                                    @error('product_size')
                                        <span style="color: red">{{ $message }}</span>
                                    @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-12">Category</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" name="id_category">
                                        <option  value="0" selected>category</option>
                                        @foreach ($parent_category as $parent_value )
                                            <option  value="{{ $parent_value->id  }}">--{{$parent_value->category_name}}--</option>
                                            @foreach ($list as $value_item)
                                                @if ($value_item->category_level == $parent_value->id)
                                                    <option  value="{{ $value_item->id  }}">----{{$value_item->category_name}}----</option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <a  class="btn btn-success send_data">Create product</a>
                                </div>
                            </div>

                            <div class="table-responsive" id="list_product_attr">
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
        
    <div class="clearfix"> </div>
    <script>
        $(document).ready(function () {
            var rs = [];
            var color =[];
            var size =[];
            var html = '';
            $('.send_data').click(function(){
                rs = [];
                color =[];
                size =[];
                html = "<table class='table'>"+
                            "<thead>"+
                                "<tr>"+
                                    "<th scope='col'>ID_Product</th>"+
                                    "<th scope='col'>Product_attr</th>"+
                                    "<th scope='col'>Qty</th>"+
                                    "<th scope='col'>Price</th>"+
                                "</tr>"+
                            "</thead>"+
                            "<tbody>";
        
        
                var product_id = $("#product_id").val();
                var j = 0;
                $(':checkbox:checked').map(function (i) {
                    rs[i]=$(this).val();
                    if ($(this).hasClass('product_size')) {
                        size[i] =$(this).val();
                    }else{
                        color[j] =$(this).val();
                        j++;
                    };
                });
        
                size.forEach(color_val => {
                    color.forEach(size_val => {
                        // console.log(color_val +'-'+ size_val); 
                        html+="<tr>"+
                                "<th scope='row' class='product_attr_id'>"+product_id+"</th>"+
                                "<th><input type='text' class='product_attr_name product_attr' name='product_attr_name[]' value ='"+color_val +"-"+ size_val+"'></th>"+
                                "<th><input type='text' class='product_attr_qty product_attr' name='product_attr_qty[]'></th>"+
                                "<th><input type='text'  class='product_attr_price product_attr' name='product_attr_price[]'></th>"+
                                "<input type='hidden'  class='product_attr_price product_attr' name='data_attr[]' value = '"+product_id+"','"+product_id+"'>"+
                            "</tr>";
                        });
                    });
        
                html+="</tbody>"+
                        "</table>"+
                    "<div class='form-group'>"+
                        "<div class='col-sm-12'>"+
                            "<button type='submit'  class='btn btn-success'>Save Product</button>"+
                        "</div>"+
                    "</div>";
        
                    $("#list_product_attr").html(html);
        
                }); 
            });
        </script>
        <style>
            .product_attr{
                border: none;
                height: 40px;
                width: 100%;
                outline: none;
            }
        </style>
    </section>
@endsection


