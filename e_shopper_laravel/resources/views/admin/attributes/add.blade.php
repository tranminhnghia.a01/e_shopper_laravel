@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        @if (session('msg'))
            <div class="alert alert-{{session('style')}}">
                {{ session('msg') }}
            </div>
      @endif
		<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Create Blog
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal form-material" id="signupForm" enctype="multipart/form-data" novalidate="novalidate" method="POST" action="{{ route('attributes.store') }}">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-12">Attribute name</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" id="inputName" name="attr_name">
                                        <option  value="color" selected>Color</option>
                                        <option  value="size" >Size</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group attr_val1">
                                <label class="col-md-12">Attribute value <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="input_val1" name="attr_value" value="">
                                    @error('category_name')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group attr_val2" style="display: none">
                                <label class="col-md-12">Attribute value <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="input_val2" name="" placeholder="Size(S,M,L,XL,...)">
                                    @error('category_name')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>


                           
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
            <div class="table-agile-info">
                    <div class="row w3-res-tb">
                        <div class="col-sm-5 m-b-xs">
                            <select class="input-sm form-control w-sm inline v-middle">
                                <option value="0">Bulk action</option>
                                <option value="1">Delete selected</option>
                                <option value="2">Bulk edit</option>
                                <option value="3">Export</option>
                            </select>
                            <button class="btn btn-sm btn-default">Apply</button>                
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive col-sm-6">
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                    <th style="width:20px;">
                                    </th>
                                    <th>Attribute</th>
                                    <th>Value</th>
                                    <th style="width:30px;"></th>
                                    <th style="width:30px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($color_list as $key=> $value )
                                <tr>
                                    <th scope="row">{{ $key+1}}</th>
                                    <td>{{ $value->attr_name }}
                                        <i class="ti-layout-width-full" style="background: {{ $value->attr_value }}"></i>
                                    </td>
                                    <td>{{ $value->attr_value }}</td>
                                    <td>
                                        <a href="{{ route('attributes.edit',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i></a>
                                        <a href="{{ route('attributes.destroy',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-times text-danger text"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive col-12">
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                    <th style="width:20px;">
                                    </th>
                                    <th>Attribute</th>
                                    <th>Value</th>
                                    <th style="width:30px;"></th>
                                    <th style="width:30px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($size_list as $key => $value )
                                    <tr>
                                        <th scope="row">{{ $key+1 }}</th>
                                        <td>{{ $value->attr_name }}</td>
                                        <td>{{ $value->attr_value }}</td>
                                        <td>
                                            <a href="{{ route('attributes.edit',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i></a>
                                            <a href="{{ route('attributes.destroy',$value->id) }}" class="active" ui-toggle-class=""><i class="fa fa-times text-danger text"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
  
        <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
        <script>
            $('#inputName').change(function(e){
                var _ip = $('#inputName').val();
                if (_ip == "color") {
                    $('.attr_val1').show();
                    $('.attr_val2').hide();
                    $('#input_val2').attr({name:''});
                    $('#input_val1').attr({name:'attr_value'});
                }else{
                    $('.attr_val2').show();
                    $('.attr_val1').hide();
                    $('#input_val1').attr({name:''});
                    $('#input_val2').attr({name:'attr_value'});
                }
                });
        </script>
            
    <div class="clearfix"> </div>

    </section>
@endsection