@extends('admin.layouts.app')
@section('content')
	<section class="wrapper">
        
        @if (session('msg'))
            <div class="alert alert-{{session('style')}}">
                {{ session('msg') }}
            </div>
      @endif
		<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Create category
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <form class="cmxform form-horizontal " id="signupForm" enctype="multipart/form-data" novalidate="novalidate" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Category Name <span style="color: red">(*)</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="category_name" value="">
                                    @error('category_name')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Avatar</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control form-control-line" name="category_image">
                                    @error('category_image')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Descriptions</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line"  name="category_des"></textarea>
                                    @error('category_des')
                                    <span style="color: red">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" name="category_status">
                                        <option  value="0">Hide</option>
                                        <option  value="1" selected >Show</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Category</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" name="category_level">
                                        <option  value="0" selected>Category</option>
                                        @foreach ($parent_category as $parent_value )
                                            <option  value="{{ $parent_value->id  }}">--{{$parent_value->category_name}}--</option>

                                            @foreach ($list as $value_item)
                                                @if ($value_item->category_level == $parent_value->id)
                                                    <option  value="{{ $value_item->id  }}">----{{$value_item->category_name}}----</option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success">Add Category</button>
                                </div>
                            </div>
                        </form>
                            
                    </div>
                </section>
            </div>
        </div>
</section>
@endsection