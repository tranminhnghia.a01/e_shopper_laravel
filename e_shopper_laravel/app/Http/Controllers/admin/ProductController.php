<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product =Product::all();
        $attr = Attribute::all();
        $product_attr =ProductAttr::all();
        $category = Category::all();
        
        return view('admin.products.list')->with(compact('product','product_attr','attr','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list = Category::all();
        $parent_category = Category::where('category_level',0)->get();
        $color_list = Attribute::where('attr_name',"color")->get();
        $size_list = Attribute::where('attr_name',"size")->get();
        return view('admin.products.add')->with(compact('list','parent_category','color_list','size_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $errors = 0;
        $id_product = $request->id_product;
        if($request->hasfile('product_image'))
        {
            foreach($request->file('product_image') as $image)
            {
                    $name = $id_product.'-'.$image->getClientOriginalName();
                $file_images[] = $name;
            }
        }
        $product_attr_qty = $request->product_attr_qty;
        $product_attr_price = $request->product_attr_price;

        $product_info = [
            'product_image' => json_encode($file_images),
            'id_product' => $request->id_product,
            'id_category' => $request->id_category,
            'product_name' => $request->product_name,
            'product_status' => $request->product_status,
            'product_price' => $request->product_price,
            'product_des' => $request->product_des,
            'product_content' => $request->product_content,
            'created_at' => now(),
        ];
        $product = Product::create($product_info);
        if ($product) {
            if($request->hasfile('product_image'))
            {
                foreach($request->file('product_image') as $image)
                {

                    $name = $product->id_product.'-'.$image->getClientOriginalName();
                    //$image->move('upload/product/', $name);
                    $path = public_path('backend/upload/products/' . $name);
                    Image::make($image->getRealPath())->save($path);
                    // $file_images[] = $name;
                }
            }
            
            foreach ($request->product_attr_name as $key => $value) {
                $product_attr = [
                    'product_id' => $product->id_product,
                    'product_attr_name' =>  $value,
                    'product_attr_qty' => $product_attr_qty[$key],
                    'product_attr_price' => $product_attr_price[$key],
                    'product_attr_status' => 1,
                ];
                $product_attr = ProductAttr::create($product_attr);
                if ($product_attr == false) {
                    $errors = 1;
                }
            }
            
        }
        if ($errors == 0) {
            $msg = "Add product success!";
            $style = "success";
        }else{
            $msg = "Add product errors!";
            $style = "danger";
        }
        // dd($product);

        return redirect()->back()->with(compact('msg','style'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = Category::all();
        $parent_category = Category::where('category_level',0)->get();
        $color_list = Attribute::where('attr_name',"color")->get();
        $size_list = Attribute::where('attr_name',"size")->get();
        $product = Product::find($id);
        $product_info = ProductAttr::where('product_id',$product->id_product)->get();
        return view('admin.products.show_info')->with(compact('list','parent_category','color_list','size_list','product','product_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $get_name_category = Category::where('id',$product->id_category)->first();
        $parent_category = Category::where('category_level',0)->get();
        $list = Category::all();


        return view('admin.products.edit')->with(compact('product','list','parent_category','get_name_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::find($id);
        $images_delete = $request->image_delete;  
        // dd($images_delete);

        $image_old =json_decode($product->product_image);
        if (!empty($images_delete)) {
            foreach ($image_old as $key => $value) {
                if (in_array($value,$images_delete)) {
                 unset($image_old[$key]);
                }
             }
        }
        
        $image_new = $image_old;

        //hinh them
        if($request->hasfile('product_image'))
        {
            foreach($request->file('product_image') as $image)
            {
                $name = $product->id_product.'-'.$image->getClientOriginalName();
                $file_images[] = $name;
            }
            $image_new = array_merge($file_images ,$image_old);
        }

        if (count($image_new)<=5 && count($image_new)> 0) {
            $data['product_image']=json_encode($image_new);
            // dd($data);
            if ($product->update($data)) {
                if($request->hasfile('product_image'))
                {
                    foreach($request->file('product_image') as $image)
                    {

                        $name =$product->id_product.'-'.$image->getClientOriginalName();
                        //$image->move('upload/product/', $name);
                        $path = public_path('backend/upload/products/'.$name);

                        Image::make($image->getRealPath())->save($path);
                        
                    }
                    if (!empty($images_delete)) {
                        foreach ($images_delete as $key => $value) 
                        {
                             unlink('backend/upload/products/'.$value);
                        }
                    }
                }
                $msg = 'Update product success!';
                $style = 'success';
            }else{
                $msg = 'Update product errors!';
                $style = 'danger';

            }
        }
        else{
            $msg = 'images errors';
            $style = 'danger';

        }
        return redirect()->back()->with(compact('msg','style'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $product =Product::find($id);
        if ($product) {
            $product_attr = ProductAttr::where('product_id',$product->id_product)->get();
            if ($product_attr) {
                $product_attr->delete();
            }
            $product->delete();
            $msg = 'delete all product success';
            $style = 'success';
        }else{
            $msg = 'delete errors';
            $style = 'danger';
        }
        return redirect()->back()->with(compact('msg','style'));
    }

}
