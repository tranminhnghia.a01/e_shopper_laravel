<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog_list = Blog::all();
        return view('admin.blogs.list')->with(compact('blog_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        
        $data = $request->all();
        $file = $request->blog_image;

        if(!empty($file)){
            $data['blog_image'] = $file->getClientOriginalName();
        }

        $data['blog_status'] = 1;
        
        $rs=Blog::create($data);
        if ($rs) {
            if (!empty($file)) {
                $file->move('admin/upload/blogs',$file->getClientOriginalName());
                
            }
            return redirect()->back()->with('msg',__('Add blog success!'));
        }
        else{
            return redirect()->back()->with('msg','Add blog errors.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Blog::find($id);
        return view('admin.blogs.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->all();
        $dataBlog=Blog::find($id);
        $file = $request->blog_image;
        $old_image = $dataBlog->blog_image;
        // dd($old_image);
        if(!empty($file)){
            $data['blog_image'] = $file->getClientOriginalName();
        }


        if ($dataBlog->update($data)) {
            $msg = "Update blog success!";
            if(!empty($file)){
                $file->move('admin/upload/blogs',$file->getClientOriginalName());
                unlink('admin/upload/blogs/'.$old_image);
            }
            return  redirect()->route('admin.blogs')->with(compact('msg'));
        }
        else{
            $msg = "Update blog error.";
            return back()->with(compact('msg'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Blog::find($id)->delete()) {
            $msg = "Delete blog success!";
            $style ="success";
        }
        else{
            $msg = "Delete blog error!";
            $style ="danger";
        }
        return redirect()->back()->with(compact('msg','style'));
    }
}
