<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Attribute;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Category::all();
        // dd($parent_category);

        return view('admin.categories.list')->with(compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list = Category::all();
        $parent_category = Category::where('category_level',0)->get();
        return view('admin.categories.add')->with(compact('list','parent_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->all();
        // dd($data);
        
        $file = $request->category_image;
        if (!empty($file)) {
            $data['category_image'] = $file->getClientOriginalName();
        }

        if (Category::create($data)) {
            if (!empty($file)) {
                $file->move(public_path('backend/upload/categories'),$file->getClientOriginalName());
            }
            return redirect()->back()->with('msg',__('Add category success!'));
        }
        else{
            return redirect()->withErrors('Add category errors.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Category::find($id);
        $list = Category::all();
        $parent_category = Category::where('category_level',0)->get();

        return view('admin.categories.edit')->with(compact('detail','list','parent_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $file = $request->category_image;
        if (!empty($file)) {
            $data['category_image'] = $file->getClientOriginalName();
        }

        $update = Category::find($id);
        $old_image = $update->category_image;
        if ($update->update($data)) {
            if (!empty($file)) {
                $file->move(public_path('backend/upload/categories'),$file->getClientOriginalName());
                unlink('backend/upload/categories'.$old_image);
            }
            return redirect()->back()->with('msg',__('Update profile success!'));
        }
        else{
            return redirect()->withErrors('Update profile errors.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
