<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id_product'=>'required',
            'product_name'=>'required',
            'product_content'=>'required',
            'product_des'=>'required',
            'product_status'=>'required',
            'product_image.*' => 'required|mimes:jpg,jpeg,png,bmp|max:2048'


        ];
    }
    public function messages()
    {       
        return[
            'required' =>':attribute không được để trống',
        ];
    }
    public function attributes()
    {
        return[
            'product_name'=>'product Name',
        ];
    }
}
