<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttr extends Model
{
    use HasFactory;
    public $timestamps =true;

    protected $table ='tbl_product_attr';

    protected $fillable =[
        'product_id',
        'product_attr_name',
        'product_attr_qty',
        'product_attr_price',
        'product_attr_status',
    ];
   
    protected $primary_key = 'id';
    
    protected $hidden = [
        '_token',
    ];
}
