<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;
    public $timestamps =true;

    protected $table ='tbl_attributes';

    protected $fillable =[
        'attr_name',
        'attr_value',
    ];
   
    protected $primary_key = 'id';
    
    protected $hidden = [
        '_token',
    ];
}
