<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPUnit\Framework\isNull;

class Product extends Model
{
    use HasFactory;
    protected $table = 'tbl_product';
    protected $fillable = [
        'id_product',
        'id_category',
        'product_name',
        'product_des',
        'product_image',
        'product_price',
        'product_content',
        'product_status',
        'created_at',
        'updated_at'
    ];
    protected $primary_key ='id';

    protected $hidden = [
        '_token',
        'product_image',
    ];

    public $timestamps = false;
}
