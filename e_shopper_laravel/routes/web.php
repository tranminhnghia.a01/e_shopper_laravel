<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend
Route::get('/', [App\Http\Controllers\frontend\HomeController::class, 'index']);

// backend
Route::get('/home', function () {
    return view('admin.dashboard');
});

Auth::routes();

Route::prefix('/admin')->group(function (){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/profile', [App\Http\Controllers\admin\UserController::class, 'index'])->name('profile');
    Route::post('/profile', [App\Http\Controllers\admin\UserController::class, 'update']);


    Route::get('/blog', [App\Http\Controllers\admin\BlogController::class, 'index'])->name('admin.blogs');
    Route::get('/blog/create', [App\Http\Controllers\admin\BlogController::class, 'create'])->name('admin.blog-create');
    Route::post('/blog/create', [App\Http\Controllers\admin\BlogController::class, 'store']);
    Route::get('/blog/edit/{id}', [App\Http\Controllers\admin\BlogController::class, 'edit'])->name('admin.blog-edit');
    Route::post('/blog/edit/{id}', [App\Http\Controllers\admin\BlogController::class, 'update']);
    Route::get('/blog/destroy/{id}', [App\Http\Controllers\admin\BlogController::class, 'destroy'])->name('admin.blog-destroy');


    Route::get('/category', [App\Http\Controllers\admin\categoryController::class, 'index'])->name('admin.categories');
    Route::get('/category/create', [App\Http\Controllers\admin\categoryController::class, 'create'])->name('admin.category-create');
    Route::post('/category/create', [App\Http\Controllers\admin\categoryController::class, 'store']);
    Route::get('/category/edit/{id}', [App\Http\Controllers\admin\categoryController::class, 'edit'])->name('admin.category-edit');
    Route::post('/category/edit/{id}', [App\Http\Controllers\admin\categoryController::class, 'update']);
    Route::get('/category/destroy/{id}', [App\Http\Controllers\admin\categoryController::class, 'destroy'])->name('admin.category-destroy');


    Route::resource('attributes',App\Http\Controllers\admin\AttributeController::class);
    Route::resource('product',App\Http\Controllers\admin\ProductController::class);

    Route::get('/product', [App\Http\Controllers\admin\ProductController::class, 'index'])->name('product.index');
    Route::get('/product/create', [App\Http\Controllers\admin\ProductController::class, 'create'])->name('product.create');
    Route::post('/product/create', [App\Http\Controllers\admin\ProductController::class, 'store']);
    Route::get('/product/edit/{id}', [App\Http\Controllers\admin\ProductController::class, 'edit'])->name('product.edit');
    Route::get('/product/show/{id}', [App\Http\Controllers\admin\ProductController::class, 'show'])->name('product.show'); 
    Route::post('/product/update/{id}', [App\Http\Controllers\admin\ProductController::class, 'update'])->name('product.update');
    Route::get('/product/destroy/{id}', [App\Http\Controllers\admin\ProductController::class, 'destroy'])->name('product.destroy');

    //Product attributes 
    Route::get('/product/show/attr/create/{id}', [App\Http\Controllers\admin\ProductAttributeController::class, 'create'])->name('product-attr.create');
    Route::post('/product/show/attr/create/{id}', [App\Http\Controllers\admin\ProductAttributeController::class, 'create']);
    Route::get('/product/show/attr/edit/{id}', [App\Http\Controllers\admin\ProductAttributeController::class, 'edit'])->name('product-attr.edit');
    Route::post('/product/show/attr/update/{id}', [App\Http\Controllers\admin\ProductAttributeController::class, 'update'])->name('product-attr.update');
    Route::get('/product/show/attr/destroy/{id}', [App\Http\Controllers\admin\ProductAttributeController::class, 'destroy'])->name('product-attr.destroy');
});


